﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CDStore.Models
{
    public class Repository
    {
        private static List<CDObject> cdLines = new List<CDObject>();

        public static IEnumerable<CDObject> showLines
        {
            get
            {
                return cdLines;
            }
        }

        public static void AddResponse(CDObject cdObject)
        {
            cdLines.Add(cdObject);
        }
    }
}
