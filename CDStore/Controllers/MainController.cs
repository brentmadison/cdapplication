﻿using CDStore.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CDStore.Controllers
{
    public class MainController : Controller
    {
        public ViewResult Index()
        {
            //CDObject test = new CDObject("Test", 60, 5.00, DateTime.Parse("9/18/2017"), 1000);
            /*CDObject test1 = new CDObject("Test", 12, 2.00, DateTime.Parse("9/18/2017"), 20000);*/

/*            List<CDObject> list = new List<CDObject>();
            list.Add(test);
            list.Add(test1);*/

            ViewBag.listEmpty = !Repository.showLines.Any<CDObject>();

            return View("Index", Repository.showLines);
            //return View("Index", list);
        }

        [HttpGet]
        public ViewResult AddCD()
        {
            return View();
        }

        [HttpPost]
        public ViewResult AddCD(CDObject data)
        {

            if (ModelState.IsValid)
            {
                Repository.AddResponse(data);
                return Index();
            }
            else
            {
                return View();
            }
        }
    }
}
