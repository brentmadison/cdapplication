﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CDStore.Models
{
    public class CDObject
    {
        [Required(ErrorMessage = "Please enter a Bank.")]
        public string Bank { get; set; }
        [Required(ErrorMessage = "Please enter a Term.")]
        public int? Term { get; set; }
        [Required(ErrorMessage = "Please enter a Rate.")]
        public double? Rate { get; set; }
        [Required(ErrorMessage = "Please enter a PurchaseDate.")]
        public DateTime? PurchaseDate { get; set; }
        [Required(ErrorMessage = "Please enter a DepositAmount.")]
        public double? DepositAmount { get; set; }

        public DateTime? MaturityDate
        {
            get
            {
                if (PurchaseDate.HasValue && Term.HasValue)
                {
                    return PurchaseDate.Value.AddMonths(Term.Value);
                }
                else
                {
                    return null;
                }
            }
        }


        //https://www.investor.gov/financial-tools-calculators/calculators/compound-interest-calculator
        //You didn't specify formula, so I did what I thought was correct.
        public double? MaturityValue
        {
            get
            {
                if (DepositAmount.HasValue && Rate.HasValue && Term.HasValue)
                {
                    return DepositAmount.Value * Math.Pow(1 + (Rate.Value / 100), (Term.Value/12));

                }
                else
                {
                    return null;
                }
            }
        }

        public CDObject()
        {

        }
        public CDObject(string bank, int term, double rate, DateTime purchaseDate, double depositAmount)
        {
            Bank = bank;
            Term = term;
            Rate = rate;
            PurchaseDate = purchaseDate;
            DepositAmount = depositAmount;

        }
    }
}